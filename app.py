import json
import requests
from flask import Flask, render_template, jsonify
from pathlib import Path
from dotenv import load_dotenv
import os
import time

app = Flask(__name__)

print("Just fir demo...")

d = Path(__file__).resolve().parents[1]
load_dotenv(rf"{d}/.env")

api_key = os.environ.get("API_KEY")
base_url = os.environ.get("BASE_URL")

# Function to write weather data to a JSON file
def write_weather_to_json(city, data):
    try:
        with open('weather_data.json', 'r+') as file:
            try:
                weather_data = json.load(file)
            except json.decoder.JSONDecodeError:
                weather_data = {}
            weather_data[city] = data
            file.seek(0)
            json.dump(weather_data, file, indent=4)
            file.truncate()
    except FileNotFoundError:
        with open('weather_data.json', 'w') as file:
            weather_data = {city: data}
            json.dump(weather_data, file, indent=4)

# Function to read weather data from a JSON file
def read_weather_from_json(city):
    try:
        with open('weather_data.json', 'r') as file:
            data = file.read()
            if data:
                try:
                    weather_data = json.loads(data)
                    return weather_data.get(city)
                except json.decoder.JSONDecodeError:
                    return None
            else:
                return None
    except FileNotFoundError:
        return None
    
# Function to fetch weather data from a API
def fetch_weather_from_api(city):
    params = {
        'q': city,
        'appid': api_key,
        'units': 'metric'
    }
    response = requests.get(base_url, params=params)
    if response.status_code == 200:
        data = response.json()
        temperature = data['main']['temp']
        humidity = data['main']['humidity']
        weather_data = {'temperature': temperature, 'humidity': humidity, 'last_modified': int(time.time())}
        write_weather_to_json(city, weather_data)
        return weather_data
    else:
        return jsonify({'error': 'Failed to fetch weather data'})

# Check weather modified last before 5 mins or not
def should_update_weather(city):
    weather_data = read_weather_from_json(city)
    if weather_data:
        last_modified = weather_data.get('last_modified', 0)
        current_time = int(time.time())
        return current_time - last_modified > 300  # 300 seconds = 5 minutes
    else:
        return True

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/weather/<city>')
def get_weather(city):
    if should_update_weather(city):
        weather_data = fetch_weather_from_api(city)
    else:
        weather_data = read_weather_from_json(city)
    return jsonify(weather_data)

if __name__ == '__main__':
    app.run(debug=True)