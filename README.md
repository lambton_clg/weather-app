# Weather Application

This is a simple Flask application that retrieves weather data for a given city using the OpenWeatherMap API.

## Features

- Fetches current weather data (temperature and humidity) for a specified city.
- Provides unit tests and integration tests to ensure the correctness of the application.

## Prerequisites

Before running the application, make sure you have the following:

- Python 3.9 installed on your system
- Docker installed (optional, if you want to run the application in a containerized environment)

## Getting Started

Follow these steps to run the application:

1. **Clone the repository:**

   ```bash
   git clone https://github.com/your-username/weather-app.git
   cd weather-app

2. **Install the dependencies:**
   ```bash
   pip install -r requirements.txt
   

3. **Set up environment variables:**
Create a .env file in the project root directory and add the following variables:
   ```bash
    API_KEY=your_openweathermap_api_key
    BASE_URL=https://api.openweathermap.org/data/2.5/weather

4. **Run the Flask application:**
   ```bash
   python app.py

5. **Open your web browser and navigate to http://localhost:5000 to access the application.**

6. **Running Unit Tests:**
To run unit tests use the following commands:
   ```bash
   python test_weather_app.py

7. **Running Integration Tests:**
To run integration tests, use the following commands:
   ```bash
   pytest

7. **Deployment:**
The application can be deployed using Docker. Ensure you have Docker installed and configured on your system.
   ```bash
   docker build -t weather-app .
   docker run -d -p 8000:5000 weather-app

The application will be accessible at **http://localhost:8000**.