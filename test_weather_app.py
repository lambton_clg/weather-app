import unittest
from app import app

class TestWeatherApp(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_home_page_loads_successfully(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

    def test_weather_details_for_london(self):
        response = self.app.get('/weather/london')
        self.assertEqual(response.status_code, 200)
        data = response.json
        self.assertIn('temperature', data)
        self.assertIn('humidity', data)

if __name__ == '__main__':
    unittest.main()