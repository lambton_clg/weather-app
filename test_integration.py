import json
import pytest
from app import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client



def test_weather_endpoint(client, mocker):
    mock_response = '{"main": {"temp": 15, "humidity": 60}}'
    mocker.patch('requests.get', return_value=MockResponse(200, mock_response))

    response = client.get('/weather/london')

    assert response.status_code == 200
    data = json.loads(response.data)
    assert 'temperature' in data
    assert 'humidity' in data
    assert data['temperature'] == 15
    assert data['humidity'] == 60

class MockResponse:
    def __init__(self, status_code, text):
        self.status_code = status_code
        self.text = text

    def json(self):
        return json.loads(self.text)